#include "InternationalRailway.h"

InternationalRailway::InternationalRailway(
    double stateBudgetSpending,
    list<string> plans,
    string railwayCompany,
    list<vector<string>> availableLines,
    string countryCode
) : Railway(stateBudgetSpending, plans, railwayCompany, availableLines),
    countryCode(countryCode) {}

void InternationalRailway::setCountryCode(string code)
{
    this->countryCode = code;
}

string InternationalRailway::getCountryCode()
{
    return countryCode;
}