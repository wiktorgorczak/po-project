#ifndef INLAND_TRANSPORT
#define INLAND_TRANSPORT
#include "Railway.h"
#include "Traffic.h"

using namespace std;

class InlandTransport: public Railway, public Traffic
{
private:
    string administrativeUnit;

public:
    InlandTransport(double, list<string>, string, list<vector<string>>, list<string>, list<string>, list<string>, list<string>, list<string>, string);
    void setAdministrativeUnit(string);
    string getAdministrativeUnit();
    double getStateBudgetSpending();
    void setStateBudgetSpending(double);
    list<string> getPlans();
    void setPlans(list<string>);
};
#endif