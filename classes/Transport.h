#include <list>
#ifndef TRANSPORT
#define TRANSPORT
#include <string>
#include <limits>
#define STATE_BUDGET 10000

using namespace std;

template<typename T>
class Transport
{
protected:
    T stateBudgetSpending;
    list<string> plans;
    virtual bool canWeAffordIt() = 0;
public:
    Transport();
    Transport(T stateBudgetSpending, list<string> plans);
    T getStateBudgetSpending();
    void setStateBudgetSpending(T stateBudgetSpending);
    list<string> getPlans();
    void setPlans(list<string> plans);
};

template<typename T>
Transport<T>::Transport(T stateBudgetSpending, list<string> plans):
        stateBudgetSpending(stateBudgetSpending),
        plans(plans) {}

template<typename T>
T Transport<T>::getStateBudgetSpending()
{
    return this->stateBudgetSpending;
}

template<typename T>
void Transport<T>::setStateBudgetSpending(T stateBudgetSpending)
{
    this->stateBudgetSpending = stateBudgetSpending;
    canWeAffordIt();
}

template<typename T>
list<string> Transport<T>::getPlans()
{
    return this->plans;
}

template<typename T>
void Transport<T>::setPlans(list<string> plans)
{
    this->plans = plans;
}


#endif