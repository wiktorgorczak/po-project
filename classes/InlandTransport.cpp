#include "InlandTransport.h"

InlandTransport::InlandTransport(
    double stateBudgetSpending,
    list<string> plans,
    string railwayCompany,
    list<vector<string>> availableLines,
    list<string> trafficCompanies,
    list<string> motorways,
    list<string> expressways,
    list<string> nationalRoads,
    list<string> voivodeshipRoads,
    string administrativeUnit
) : Railway(stateBudgetSpending, plans, railwayCompany, availableLines),
    Traffic(stateBudgetSpending, plans,
            trafficCompanies,
            motorways,
            expressways,
            nationalRoads,
            voivodeshipRoads),
    administrativeUnit(administrativeUnit) {}

void InlandTransport::setAdministrativeUnit(string administrativeUnit)
{
    this->administrativeUnit = administrativeUnit;
}

list<string> InlandTransport::getPlans()
{
    return Traffic::getPlans();
}

string InlandTransport::getAdministrativeUnit()
{
    return this->administrativeUnit;
}

double InlandTransport::getStateBudgetSpending()
{
    return Traffic::getStateBudgetSpending();
}

void InlandTransport::setStateBudgetSpending(double amount) {
    Traffic::setStateBudgetSpending(amount);
}

void InlandTransport::setPlans(list<string> plans) {
    Traffic::setPlans(plans);
}
