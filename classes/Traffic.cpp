#include "Traffic.h"

Traffic::Traffic(
    double stateBudgetSpendings,
    list<string> plans,
    list<string> trafficCompanies,
    list<string> motorways,
    list<string> expressways,
    list<string> nationalRoads,
    list<string> voivodeshipRoads) : 
    Transport(stateBudgetSpendings, plans),
    trafficCompanies(trafficCompanies),
    motorways(motorways),
    expressways(expressways),
    nationalRoads(nationalRoads),
    voivodeshipRoads(voivodeshipRoads) {

    if(canWeAffordIt()) {
        cutTheRibbon();
    }
}

void Traffic::cutTheRibbon()
{
//    cout << "Dodano!" << endl;
}

void Traffic::addMotorway(string motorway)
{
    this->motorways.push_back(motorway);
    cout << "Otwarto nowa autostrade!" << endl;
    cutTheRibbon();
}

void Traffic::addExpressway(string expressway)
{
    this->expressways.push_back(expressway);
    cout << "Otwarto nowa droge ekspresowa!" << endl;
    cutTheRibbon();
}

void Traffic::addNationalRoad(string nationalRoad)
{
    this->nationalRoads.push_back(nationalRoad);
    cout << "Otwarto nowa droge krajowa!" << endl;
    cutTheRibbon();
}

void Traffic::addVoivodeshipRoad(string voivodeshipRoad)
{
    this->voivodeshipRoads.push_back(voivodeshipRoad);
    cout << "Otwarto nowa droge wojewodzka!" << endl;
    cutTheRibbon();
}

void Traffic::launchATenderAndWin(string company)
{
    this->trafficCompanies.push_back(company);
    cout << "Firma " << company << " wygrala przetarg!";
}

list<string> Traffic::getTrafficCompanies()
{
    return this->trafficCompanies;
}

list<string> Traffic::getMotorways()
{
    return this->motorways;
}

list<string> Traffic::getExpressways()
{
    return this->expressways;
}

list<string> Traffic::getNationalRoads()
{
    return this->nationalRoads;
}

list<string> Traffic::getVoivodeshipRoads()
{
    return this->voivodeshipRoads;
}

bool Traffic::canWeAffordIt()
{
    if(stateBudgetSpending < STATE_BUDGET)
    {
//        cout << "Nowa droga! Stac nas jeeeeej"<<endl;
        return true;
    }
    else
    {
        cout << "WARNING: Za duze wydatki!" << endl;
        return false;
    }
}