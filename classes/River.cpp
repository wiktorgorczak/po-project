#include "River.h"

River::River(
            double stateBudgetSpending,
            list<string> plans,
            list<string> waterCompanies,
            string river) :
            Water(stateBudgetSpending, plans, waterCompanies),
            river(river) {}

void River::setRiver(string river)
{
    this->river = river;
}

string River::getRiver()
{
    return this->river;
}