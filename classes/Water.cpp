#include "Water.h"

Water::Water(double stateBudgetSpendings, 
    list<string> plans,
    list<string> waterCompanies) :
    Transport(stateBudgetSpendings, plans),
    waterCompanies(waterCompanies) {}

bool Water::canWeAffordIt()
{
    if(stateBudgetSpending < STATE_BUDGET)
    {
        cout <<"Stac nas!";
        return true;
    }
    else
    {
        cout << "Komu przeszkadza rozwoj infrastruktury wodnej?";
        return false;
    }
}

list<string> Water::getWaterCompanies()
{
    return this->waterCompanies;
}

void Water::launchATenderAndWin(string company)
{
    cout << "Firma " << company << " wygrala przetarg na rozwoj infrastruktury wodnej!";
    waterCompanies.push_back(company);
}