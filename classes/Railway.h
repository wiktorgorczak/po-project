#ifndef RAILWAY
#define RAILWAY
#include <list>
#include <vector>
#include <string>
#include <iostream>
#include "Transport.h"

using namespace std;

class Railway : public Transport<double>
{
private:
    string railwayCompany;
    list<vector<string>> availableLines;

protected:
    bool canWeAffordIt();

public:
    Railway(double, list<string>, string, list<vector<string>>);
    void setRailwayCompany(string);
    string getRailwayCompany();
    list<vector<string>> getAvailableLines();
    bool isAvailableLine(string, string);
    void addLine(string, string);
};
#endif