#include "Sea.h"

Sea::Sea(
            double stateBudgetSpending,
            list<string> plans,
            list<string> waterCompanies,
            string sea) :
            Water(stateBudgetSpending, plans, waterCompanies),
            sea(sea) {}

void Sea::setSea(string sea)
{
    this->sea = sea;
}

string Sea::getSea()
{
    return this->sea;
}