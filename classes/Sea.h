#ifndef SEA
#define SEA
#include "Water.h"

class Sea : public Water
{
private:
    string sea;

public:
    Sea(double, list<string>, list<string>, string);
    void setSea(string);
    string getSea();
};
#endif