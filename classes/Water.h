#ifndef WATER
#define WATER
#include <list>
#include <string>
#include <iostream>
#include "Transport.h"

using namespace std;

class Water : public Transport<double>
{
private:
    list<string> waterCompanies;
    bool canWeAffordIt();

public:
    Water(double, list<string>, list<string>);
    list<string> getWaterCompanies();
    void launchATenderAndWin(string);
};
#endif