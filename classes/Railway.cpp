#include "Railway.h"

Railway::Railway(double stateBudgetSpending,
    list<string> plans,
    string railwayCompany,
    list<vector<string>> availableLines) :

    Transport(stateBudgetSpending, plans),
    railwayCompany(railwayCompany),
    availableLines(availableLines) {


}

void Railway::setRailwayCompany(string railwayCompany)
{
    this->railwayCompany = railwayCompany;
    cout << "Przetarg tym razem wygrywa: " << railwayCompany << endl;
}

string Railway::getRailwayCompany()
{
    return this->railwayCompany;
}

list<vector<string>> Railway::getAvailableLines()
{
    return this->availableLines;
}

bool Railway::isAvailableLine(string from, string to)
{
    list<vector<string>>::iterator it;
    for(it = availableLines.begin(); it != availableLines.end(); ++it)
    {
        if(from == it->at(0) && to == it->at(1))
            return true;
    }
    return false;
}

void Railway::addLine(string from, string to)
{
    vector<string> line = {from, to};
    this->availableLines.push_back(line);
}

bool Railway::canWeAffordIt()
{
    if(stateBudgetSpending <= STATE_BUDGET)
    {
//        cout << "Stac nas na nowe koleje!" << endl;
        return true;
    }
    else
    {
        cout << "WARNING: Za duze wydatki!" << endl;
        return false;
    }
}