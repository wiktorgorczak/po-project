#ifndef TRAFFIC
#define TRAFFIC
#include <list>
#include <string>
#include <iostream>
#include "Transport.h"

using namespace std;

class Traffic : public Transport<double>
{
private:
    list<string> trafficCompanies;
    list<string> motorways;
    list<string> expressways;
    list<string> nationalRoads;
    list<string> voivodeshipRoads;
    void cutTheRibbon();
protected:
    bool canWeAffordIt();

public:
    Traffic(double, list<string>, list<string>, list<string>, list<string>, list<string>, list<string>);
    list<string> getTrafficCompanies();
    list<string> getMotorways();
    list<string> getExpressways();
    list<string> getNationalRoads();
    list<string> getVoivodeshipRoads();
    void addMotorway(string);
    void addExpressway(string);
    void addNationalRoad(string);
    void addVoivodeshipRoad(string);
    void launchATenderAndWin(string);
};
#endif