#include "InternationalTraffic.h"

InternationalTraffic::InternationalTraffic(
    double stateBudgetSpending,
    list<string> plans,
    list<string> trafficCompanies,
    list<string> motorways,
    list<string> expressways,
    list<string> nationalRoads,
    list<string> voivodeshipRoads,
    string state,
    list<string> borderCrossings,
    list<string> schengen
) : Traffic(stateBudgetSpending,
            plans,
            trafficCompanies,
            motorways,
            expressways,
            nationalRoads,
            voivodeshipRoads),
    state(state),
    borderCrossings(borderCrossings),
    schengen(schengen) {}

void InternationalTraffic::setState(string state)
{
    this->state = state;
}

void InternationalTraffic::setBorderCrossings(list<string> borderCrossings)
{
    this->borderCrossings = borderCrossings;
}

void InternationalTraffic::setSchengen(list<string> schengen)
{
    this->schengen = schengen;
}

string InternationalTraffic::getState()
{
    return this->state;
}

list<string> InternationalTraffic::getBorderCrossings()
{
    return this->borderCrossings;
}

list<string> InternationalTraffic::getSchengen()
{
    return this->schengen;
}

bool InternationalTraffic::isPassportRequired(string state)
{
    list<string>::iterator it;
    for(it = schengen.begin(); it != schengen.end(); ++it)
    {
        if(state == *it)
            return false;
    }
    return true;
}
