#ifndef INTERNATIONAL_RAILWAY
#define INTERNATIONAL_RAILWAY
#include "Railway.h"

using namespace std;

class InternationalRailway : public Railway
{
private:
    string countryCode;

public:
    InternationalRailway(double, list<string>, string, list<vector<string>>, string);
    void setCountryCode(string);
    string getCountryCode();

};
#endif