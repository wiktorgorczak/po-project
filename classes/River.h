#ifndef RIVER
#define RIVER
#include "Water.h"

class River : public Water
{
private:
    string river;

public:
    River(double, list<string>, list<string>, string);
    void setRiver(string);
    string getRiver();
};
#endif