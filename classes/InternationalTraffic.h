#ifndef INTERNATIONAL_TRAFFIC
#define INTERNATIONAL_TRAFFIC
#include "Traffic.h"

class InternationalTraffic : public Traffic
{
private:
    string state;
    list<string> borderCrossings;
    list<string> schengen;

public:
    InternationalTraffic(double, list<string>, list<string>, list<string>, list<string>, list<string>, list<string>, string, list<string>, list<string>);
    void setState(string);
    void setBorderCrossings(list<string>);
    void setSchengen(list<string>);
    string getState();
    list<string> getBorderCrossings();
    list<string> getSchengen();
    bool isPassportRequired(string);
};
#endif