#include <iostream>
#include <map>
#include <utility>
#include <endpoints/WaterEndpoint.h>
#include <endpoints/TrafficEndpoint.h>
#include <endpoints/RailwayEndpoint.h>
#include "endpoints/Endpoint.h"
#include "endpoints/InternationalRailwayEndpoint.h"
#include "endpoints/InlandTransportEndpoint.h"
#include "endpoints/InternationalTrafficEndpoint.h"
#include "endpoints/RiverEndpoint.h"
#include "endpoints/SeaEndpoint.h"
#include "endpoints/MainEndpoint.h"

using namespace std;

vector<string> parseCommand(string command) {
    vector<string> parsed;
    stringstream ss(command);
    while(ss.good()) {
        string substr;
        getline(ss, substr, ' ');
        parsed.push_back(substr);
    }

    return parsed;
}

void TREE() {
    cout << "Transport\n\tRailway\n\t\tInternationalRailway\n\t\tInlandTransport (1)\n\tTraffic\n\t\tInlandTransport (2)\n\t\tInternationalTraffic\n\tWater\n\t\tRiver\n\t\tSea" << endl;
}

void DIR(string endpoints[], int size) {
    cout << "\tAvailable endpoints: " << endl;
    for(int i = 0; i < size; i++) {
        cout << "\t\t" << endpoints[i] << endl;
    }
}

int main()
{
    cout << "Hi! Available commands: DIR, CD, MDO, MO, DO, SHOW, SAVE, READ, TREE, EXIT: " << endl;

    InternationalRailwayEndpoint *internationalRailwayEndpoint = new InternationalRailwayEndpoint();
    InlandTransportEndpoint *inlandTransportEndpoint = new InlandTransportEndpoint();
    InternationalTrafficEndpoint *internationalTrafficEndpoint = new InternationalTrafficEndpoint();
    RiverEndpoint *riverEndpoint = new RiverEndpoint();
    SeaEndpoint *seaEndpoint = new SeaEndpoint();
    vector<Endpoint*> endpoints{internationalRailwayEndpoint,
            inlandTransportEndpoint,
            internationalTrafficEndpoint,
            riverEndpoint,
            seaEndpoint};

    MainEndpoint *mainEndpoint = new MainEndpoint(endpoints);
    vector<Endpoint*> waterEndpoints {
        riverEndpoint,
        seaEndpoint
    };
    vector<Endpoint*> railwayEndpoints {
        inlandTransportEndpoint,
        internationalRailwayEndpoint
    };
    vector<Endpoint*> trafficEndpoints {
        inlandTransportEndpoint,
        internationalTrafficEndpoint
    };

    WaterEndpoint *waterEndpoint = new WaterEndpoint(waterEndpoints);
    TrafficEndpoint *trafficEndpoint = new TrafficEndpoint(trafficEndpoints);
    RailwayEndpoint *railwayEndpoint = new RailwayEndpoint(railwayEndpoints);


    map<string, Endpoint*> endpointMap;
    endpointMap.insert(pair<string, Endpoint*>("InternationalRailway", internationalRailwayEndpoint));
    endpointMap.insert(pair<string, Endpoint*>("InternationalTraffic", internationalTrafficEndpoint));
    endpointMap.insert(pair<string, Endpoint*>("InlandTransport", inlandTransportEndpoint));
    endpointMap.insert(pair<string, Endpoint*>("River", riverEndpoint));
    endpointMap.insert(pair<string, Endpoint*>("Sea", seaEndpoint));
    endpointMap.insert(pair<string, Endpoint*>("Transport", mainEndpoint));
    endpointMap.insert(pair<string, Endpoint*>("Railway", railwayEndpoint));
    endpointMap.insert(pair<string, Endpoint*>("Water", waterEndpoint));
    endpointMap.insert(pair<string, Endpoint*>("Traffic", trafficEndpoint));



    // prompt(mainEndpoint, endpointMap);
    Endpoint* current = mainEndpoint;
    //map<string, Endpoint*> endpoints = endpointMap;
    bool prompt = true;
    while(prompt) {
        string command;
        cout << "You are in " + current->getName() + ": Enter command: ";
        getline(cin, command);

        vector<string> args = parseCommand(command);

        if(args.size() == 0) {
            continue;
        }

        if(args[0] == "CD") {
            if(args.size() != 2) {
                cout << "CD takes exactly one argument!" << endl;
            } else {
                string destination = args[1];

                if(current->getName() == "Transport" ||
                        current->getName() == "Railway" ||
                        current->getName() == "Water" ||
                        current->getName() == "Traffic"
                    ) {
                    if(endpointMap.count(destination) > 0) {
                        current = endpointMap.at(destination);
                    } else {
                        cout << "No such class!" << endl;
                    }
                } else if(destination == "../") {
                    //prompt(endpointMap.at("MainEndpoint"), endpoints);
                    current = endpointMap.at("Transport");
                } else if(destination == "./") {
                    //nothing happens
                } else {
                    cout << "You are not in the main directory!" << endl;
                }
            }

        } else if(args[0] == "DIR") {
            if(args.size() != 1) {
                cout << "DIR command does not take any parameters!" << endl;
            } else {
                current->showAll();
            }
        } else if(args[0] == "MO") {
            if(args.size() != 1) {
                cout << "MO command does not take any parameters!" << endl;
            } else {
                current->createObject();
            }
        } else if(args[0] == "EXIT") {
            if(args.size() != 1) {
                cout << "EXIT command does not take any parameters!" << endl;
            } else {
                prompt = false;
            }
        } else if(args[0] == "TREE") {
            if(args.size() != 1) {
                cout << "TREE command does not take any parameters!" << endl;
            } else {
                TREE();
            }
        } else if(args[0] == "DO") {
            if(args.size() != 2) {
                cout << "DO takes exactly one argument (object's id)!" << endl;
            } else {
                int id = stoi(args[1]);
                current->deleteObject(id);
            }
        } else if(args[0] == "MDO") {
            if(args.size() != 2) {
                cout << "MDO takes exactly one argument (object's id)!" << endl;
            } else {
                int id = stoi(args[1]);
                current->modifyObject(id);
            }
        } else if(args[0] == "SHOW") {
            if(args.size() != 2) {
                cout << "SHOW takes exactly one argument (object's id)!" << endl;
            } else {
                int id = stoi(args[1]);
                current->showObject(id);
            }
        } else if(args[0] == "SAVE") {
            if(args.size() != 2) {
                cout << "SAVE takes exactly one argument (filename)!" << endl;
            } else {
                current->saveToFile(args[1]);
            }
        } else if(args[0] == "READ") {
            if(args.size() != 2) {
                cout << "READ takes exactly one argument (filename)!" << endl;
            } else {
                current->readFromFile(args[1]);
            }
        } else {
            cout << "Unknown command!" << endl;
        }
    }

    delete(mainEndpoint);
    delete(internationalRailwayEndpoint);
    delete(inlandTransportEndpoint);
    delete(internationalTrafficEndpoint);
    delete(riverEndpoint);
    delete(seaEndpoint);
    return EXIT_SUCCESS;
}