#ifndef ENDPOINT
#define ENDPOINT
#include<string>
#include<iostream>
#include<sstream>
#include<list>
#include<vector>
#include <fstream>

using namespace std;
class Endpoint
{
protected:
    void insertStringsToList(list<string> &lst, string content, char separator);
    void insertStringsToVector(vector<string> &vec, string content, char separator);
    string convertListToString(list<string> &lst, char separator);
    string convertVectorToString(vector<string> &vec, char separator);
    void parseFromStringToListOfVectors(list<vector<string>> &lst, string content, char listSeparator, char vecSeparator);
    string fileToString(string filename);
    void stringToFile(string filename, string content);

public:
    virtual string getName() = 0;
    virtual void createObject() = 0;
    virtual void deleteObject(int) = 0;
    virtual void modifyObject(int) = 0;
    virtual void showObject(int) = 0;
    virtual void showAll() = 0;
    virtual void saveToFile(string) = 0;
    virtual void readFromFile(string) = 0;
};
#endif