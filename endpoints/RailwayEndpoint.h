//
// Created by wiktor on 10.12.2019.
//

#ifndef PROJEKT_RAILWAYENDPOINT_H
#define PROJEKT_RAILWAYENDPOINT_H


#include "MainEndpoint.h"

class RailwayEndpoint : public MainEndpoint {
private:
    vector<Endpoint*> endpoints;
public:
    RailwayEndpoint(vector<Endpoint*> endpoints);
    string getName();
};


#endif //PROJEKT_RAILWAYENDPOINT_H
