//
// Created by wiktor on 24.11.2019.
//

#ifndef PROJEKT_MAINENDPOINT_H
#define PROJEKT_MAINENDPOINT_H
#include "Endpoint.h"

class MainEndpoint : public Endpoint
{
private:
    vector<Endpoint*> endpoints;

public:
    MainEndpoint(vector<Endpoint*> endpoints);
    string getName();
    void createObject();
    void deleteObject(int);
    void showObject(int);
    void modifyObject(int);
    void showAll();
    void saveToFile(string);
    void readFromFile(string);
};

#endif //PROJEKT_MAINENDPOINT_H
