#include "Endpoint.h"

void Endpoint::insertStringsToList(list<string> &lst, string content, char separator) {
    stringstream ss(content);
    while(ss.good()) {
        string substr;
        getline(ss, substr, separator);
        lst.push_back(substr);
    }
}

void Endpoint::insertStringsToVector(vector<string> &vec, string content, char separator) {
    stringstream ss(content);
    while(ss.good()) {
        string substr;
        getline(ss, substr, separator);
        vec.push_back(substr);
    }
}

string Endpoint::convertListToString(list<string> &lst, char separator) {
    string content = "";
    list<string>::iterator it;
    for(it = lst.begin(); it != lst.end(); ++it) {
        content = content + *it + separator;
    }
    return content.substr(0, content.size() - 1);;
}

string Endpoint::convertVectorToString(vector<string> &vec, char separator) {
    string content = "";
    vector<string>::iterator it;
    for(it = vec.begin(); it != vec.end(); ++it) {
        content = content + *it + separator;
    }
    return content.substr(0, content.size() - 1);
}


void Endpoint::parseFromStringToListOfVectors(list<vector<string>> &lst, string content, char listSeparator, char vecSeparator)
{
    stringstream ss(content);
    while(ss.good())
    {
        vector<string> vec;
        string vecStr;
        getline(ss, vecStr, listSeparator);

        insertStringsToVector(vec, vecStr, vecSeparator);

        lst.push_back(vec);
    }
}

string Endpoint::fileToString(string filename) {
    ifstream source;
    source.open(filename);
    if(!source.is_open()) {
        throw string("Cannot open the given file!");
        return "";
    }
    string output = "";
    while(!source.eof())
    {
        string tmp;
        getline(source, tmp);
        output += tmp + '\n';
    }
    output = output.substr(0, output.size() - 1);
    source.close();

    return output;
}

void Endpoint::stringToFile(string filename, string content) {
    ofstream dest;
    dest.open(filename);
    if(!dest.is_open()) {
        throw string("Cannot open the given file!");
    }

    dest << content;
    dest.close();
}
