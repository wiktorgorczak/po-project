//
// Created by wiktor on 10.12.2019.
//

#include "RailwayEndpoint.h"

RailwayEndpoint::RailwayEndpoint(vector<Endpoint *> endpoints) : MainEndpoint(endpoints) {}

string RailwayEndpoint::getName() {
    return "Railway";
}