//
// Created by wiktor on 10.12.2019.
//

#ifndef PROJEKT_TRAFFICENDPOINT_H
#define PROJEKT_TRAFFICENDPOINT_H


#include "Endpoint.h"
#include "MainEndpoint.h"

class TrafficEndpoint : public MainEndpoint {
private:
    vector<Endpoint*> endpoints;
public:
    TrafficEndpoint(vector<Endpoint*> endpoints);
    string getName();

};


#endif //PROJEKT_TRAFFICENDPOINT_H
