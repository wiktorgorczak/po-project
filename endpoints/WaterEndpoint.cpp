//
// Created by wiktor on 10.12.2019.
//

#include "WaterEndpoint.h"

WaterEndpoint::WaterEndpoint(vector<Endpoint *> endpoints) : MainEndpoint(endpoints){}

string WaterEndpoint::getName() {
    return "Water";
}