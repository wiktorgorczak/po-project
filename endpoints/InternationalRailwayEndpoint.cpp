#include "InternationalRailwayEndpoint.h"

InternationalRailway InternationalRailwayEndpoint::generateObject() {
    double spending;
    list<string> plans;
    string railwayCompany;
    list<vector<string>> availableLines;
    string countryCode;

    cout << "Enter state budget spending: " << endl;
    cin >> spending;

    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    cout << "Enter plans separated by semicolon (planA;planB;planC): " << endl;
    string plansStr;
    getline(cin, plansStr);
    insertStringsToList(plans, plansStr, ';');

    cout << "Enter railway company: " << endl;
    getline(cin, railwayCompany);

    cout << "Enter lines (stationA,stationB;stationA,stationB;...)" << endl;
    string availableLinesStr;
    getline(cin, availableLinesStr);

    list<string> availableLinesTmp;
    insertStringsToList(availableLinesTmp, availableLinesStr, ';');

    list<string>::iterator it;
    for(it = availableLinesTmp.begin(); it != availableLinesTmp.end(); ++it) {
        vector<string> vec;
        insertStringsToVector(vec, *it, ',');
        availableLines.push_back(vec);
    }

    cout << "Enter country code: " << endl;
    cin>>countryCode;

    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    InternationalRailway object(spending, plans, railwayCompany, availableLines, countryCode);
    return object;
}

void InternationalRailwayEndpoint::createObject()
{
    cout << "Creating new object InternationlRailway\n========" << endl;
    this->internationalRailways.push_back(generateObject());
}

void InternationalRailwayEndpoint::deleteObject(int pos)
{
    if(pos >= this->internationalRailways.size()) {
        cout << "No such object!" << endl;
        return;
    }
    vector<InternationalRailway> *objects = &this->internationalRailways;
    objects->erase(objects->begin() + pos);
}

void InternationalRailwayEndpoint::modifyObject(int pos)
{
    if(pos >= this->internationalRailways.size()) {
        cout << "No such object!" << endl;
        return;
    }
    cout << "Modifying object from position " << pos << "\n========" << endl;
    //this->internationalRailways[pos] = generateObject();
    InternationalRailway modified = generateObject();
    this->internationalRailways[pos].setCountryCode(modified.getCountryCode());
    this->internationalRailways[pos].setPlans(modified.getPlans());
    this->internationalRailways[pos].setStateBudgetSpending(modified.getStateBudgetSpending());
    this->internationalRailways[pos].setRailwayCompany(modified.getRailwayCompany());
}

void InternationalRailwayEndpoint::showObject(int pos)
{
    if(pos >= this->internationalRailways.size()) {
        cout << "No such object!" << endl;
        return;
    }

    InternationalRailway *object = &this->internationalRailways.at(pos);
    cout << "Information about object no. " << pos << endl;
    cout << "Country code: " << object->getCountryCode() << endl;
    cout << "Railway company: " << object->getRailwayCompany() << endl;
    cout << "\tAvailable lines:" << endl;

    list<vector<string>>::iterator linesIterator;
    list<vector<string>> availableLines = object->getAvailableLines();

    for(linesIterator = availableLines.begin(); linesIterator != availableLines.end(); ++linesIterator) {
        cout << "\t\tA: " << linesIterator->at(0) << " B: " << linesIterator->at(1) << endl;
    }

    cout << "\t Plans: " <<endl;
    list<string> plans = object->getPlans();
    list<string>::iterator plansIterator;

    for(plansIterator = plans.begin(); plansIterator != plans.end(); ++plansIterator) {
        cout << "\t\tPlan: " << *plansIterator << endl;
    }
}

void InternationalRailwayEndpoint::showAll()
{
    int index = 0;
    cout << "All objects of this class: " << endl;
    vector<InternationalRailway>::iterator it;
    for(int i = 0; i < internationalRailways.size(); i++)
    {
        cout << "No. " << i << "\n";
        showObject(i);
    }
}

void InternationalRailwayEndpoint::saveToFile(string filename)
{
    string content = "";
    for(InternationalRailway internationalRailway : internationalRailways)
    {
        content += parseToString(internationalRailway);
    }
    try {
        stringToFile(filename, content);
    } catch(string message) {
        cout << message << endl;
    }
}

void InternationalRailwayEndpoint::readFromFile(string filename)
{
    string output;
    try {
       output = fileToString(filename);
    } catch(string message) {
        cout << message << endl;
        return;
    }

    vector<InternationalRailway> retrieved = parseFromString(output);
    for(int i = 0; i < retrieved.size(); i++)
    {
        internationalRailways.push_back(retrieved[i]);
    }
}

string InternationalRailwayEndpoint::parseToString(InternationalRailway &objectToParse)
{
    string content = "TYPE:InternationalRailway\n";
    content += "STATE_BUDGET_SPENDING:" + to_string(objectToParse.getStateBudgetSpending()) + "\n";
    content += "PLANS:";
    list<string> plans = objectToParse.getPlans();
    list<string>::iterator plansIterator;
    for(plansIterator = plans.begin(); plansIterator != plans.end(); ++plansIterator)
    {
        content += *plansIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "RAILWAY_COMPANY:" + objectToParse.getRailwayCompany() + "\n";
    content += "COUNTRY_CODE:" + objectToParse.getCountryCode() + "\n";
    content += "AVAILABLE_LINES:";

    list<vector<string>>::iterator linesIterator;
    list<vector<string>> availableLines = objectToParse.getAvailableLines();
    for(linesIterator = availableLines.begin();
        linesIterator != availableLines.end();
        ++linesIterator)
    {
        content += linesIterator->at(0) + "," + linesIterator->at(1) + ";";
    }
    content = content.substr(0, content.size() - 1);

    content += "\nEND:END\n";
    return content;
}

vector<InternationalRailway> InternationalRailwayEndpoint::parseFromString(string content) {
    vector<InternationalRailway> result;
    double spending;
    list<string> plans;
    string railwayCompany, countryCode;
    list<vector<string>> availableLines;

    stringstream ss(content);
    bool ignore = true;
    while(ss.good())
    {
        string substr;
        getline(ss, substr, '\n');

        if(!ignore)
        {
            stringstream substrSs(substr);
            string key, value;
            getline(substrSs, key, ':');
            getline(substrSs, value, ':');

            if(key == "STATE_BUDGET_SPENDING")
            {
                spending = stod(value);
            }
            else if(key == "COUNTRY_CODE")
            {
                countryCode = value;
            }
            else if(key == "RAILWAY_COMPANY")
            {
                railwayCompany = value;
            }
            else if(key == "PLANS")
            {
                insertStringsToList(plans, value, ';');
            }
            else if(key == "AVAILABLE_LINES")
            {
                parseFromStringToListOfVectors(availableLines, value, ';', ',');
            }
            else if(key == "END")
            {
                InternationalRailway parsed(spending, plans, railwayCompany, availableLines, countryCode);
                result.push_back(parsed);

                spending = 0;
                plans.clear();
                availableLines.clear();
                railwayCompany = "";
                countryCode = "";
            }
            else
            {
                ignore = true;
            }

        }
        if(substr == "TYPE:InternationalRailway")
            ignore = false;
    }

    return result;
}

InternationalRailwayEndpoint::InternationalRailwayEndpoint() 
{
    
}

string InternationalRailwayEndpoint::getName()
{
    return "InternationalRailway";
}