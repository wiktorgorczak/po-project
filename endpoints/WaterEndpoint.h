//
// Created by wiktor on 10.12.2019.
//

#ifndef PROJEKT_WATERENDPOINT_H
#define PROJEKT_WATERENDPOINT_H


#include "MainEndpoint.h"

class WaterEndpoint : public MainEndpoint{
private:
    vector<Endpoint*> endpoints;
public:
    WaterEndpoint(vector<Endpoint*> endpoints);
    string getName();
};


#endif //PROJEKT_WATERENDPOINT_H
