//
// Created by wiktor on 23.11.2019.
//

#include "RiverEndpoint.h"

RiverEndpoint::RiverEndpoint() {}

string RiverEndpoint::parseToString(River &objectToParse) {
    string content = "TYPE:River\n";
    content += "STATE_BUDGET_SPENDING:" + to_string(objectToParse.getStateBudgetSpending()) + "\n";

    list<string>::iterator listIterator;
    content += "WATER_COMPANIES:";
    list<string> companies = objectToParse.getWaterCompanies();
    for(listIterator = companies.begin(); listIterator != companies.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "PLANS:";
    list<string> plans = objectToParse.getPlans();
    for(listIterator = plans.begin(); listIterator != plans.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content += "\nRIVER:" + objectToParse.getRiver();
    content = content.substr(0, content.size() - 1);

    content += "\nEND:END\n";
    return content;
}

vector<River> RiverEndpoint::parseFromString(string content) {
    vector<River> result;
    double spending;
    list<string> plans, waterCompanies;
    string river;

    stringstream ss(content);
    bool ignore = true;
    while(ss.good()) {
        string substr;
        getline(ss, substr, '\n');

        if (!ignore) {
            stringstream substrSs(substr);
            string key, value;
            getline(substrSs, key, ':');
            getline(substrSs, value, ':');

            if (key == "STATE_BUDGET_SPENDING") {
                spending = stod(value);
            } else if (key == "RIVER") {
                river = value;
            } else if (key == "PLANS") {
                insertStringsToList(plans, value, ';');
            } else if (key == "WATER_COMPANIES") {
                insertStringsToList(waterCompanies, value, ';');
            } else if (key == "END") {
                River parsed(spending, plans, waterCompanies, river);
                result.push_back(parsed);

                spending = 0;
                plans.clear();
                waterCompanies.clear();
                river = "";
            } else {
                ignore = true;
            }

        }
        if (substr == "TYPE:River")
            ignore = false;
    }
    return result;
}

River RiverEndpoint::generateObject() {
    double spending;
    list<string> plans, waterCompanies;
    string river;

    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    cout << "Enter state budget spending: " << endl;
    cin >> spending;

    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    cout << "Enter plans separated by semicolon (planA;planB;planC): " << endl;
    string plansStr;
    getline(cin, plansStr);
    insertStringsToList(plans, plansStr, ';');

    cout << "Enter river: " << endl;
    getline(cin, river);

    cout << "Enter water companies separated by semicolon (Company1;Company2;Company3): " << endl;
    string companiesStr;
    getline(cin, companiesStr);
    insertStringsToList(waterCompanies, companiesStr, ';');

    River object(spending, plans, waterCompanies, river);
    return object;
}

string RiverEndpoint::getName() {
    return "River";
}

void RiverEndpoint::createObject() {
    cout << "Creating new object River\n=======" << endl;
    this->riverObjects.push_back(generateObject());
}

void RiverEndpoint::deleteObject(int pos) {
    if(pos >= this->riverObjects.size()) {
        cout << "No such object!" << endl;
        return;
    }

    vector<River> *objects = &this->riverObjects;
    objects->erase(objects->begin() + pos);
}

void RiverEndpoint::modifyObject(int pos) {
    if(pos >= this->riverObjects.size()) {
        cout << "No such object!" << endl;
        return;
    }

    cout << "Modifying object from position: " << pos << "\n========";
//    this->riverObjects[pos] = generateObject();
    River modified = generateObject();

    this->riverObjects[pos].setStateBudgetSpending(modified.getStateBudgetSpending());
    this->riverObjects[pos].setPlans(modified.getPlans());
    this->riverObjects[pos].setRiver(modified.getRiver());
}

void RiverEndpoint::showObject(int pos) {
    if(pos >= this->riverObjects.size()) {
        cout <<"No such object!" << endl;
        return;
    }

    River *object = &this->riverObjects.at(pos);
    cout << "Information about object no. " << pos << endl;
    cout << "River: " << object->getRiver() << endl;

    list<string>::iterator it;

    cout << "\t Water companies: " <<endl;
    list<string> companies = object->getWaterCompanies();
    for(it = companies.begin(); it != companies.end(); ++it) {
        cout << "\t\tCompany: " << *it << endl;
    }

    cout << "\t Plans: " <<endl;
    list<string> plans = object->getPlans();
    for(it = plans.begin(); it != plans.end(); ++it) {
        cout << "\t\tPlan: " << *it << endl;
    }
}

void RiverEndpoint::showAll() {
    cout << "All objects of this class: " << endl;
    vector<River>::iterator it;

    for(int i = 0; i < riverObjects.size(); i++) {
        cout << "No. " << i << "\n";
        showObject(i);
    }
}

void RiverEndpoint::saveToFile(string filename) {
    string content = "";
    for(River river : riverObjects) {
        content += parseToString(river);
    }

    try {
        stringToFile(filename, content);
    } catch(string message) {
        cout << message << endl;
    }
}

void RiverEndpoint::readFromFile(string filename) {
    string output;
    try {
        output = fileToString(filename);
    } catch(string message) {
        cout << message << endl;
        return;
    }

    vector<River> retrieved = parseFromString(output);
    for(int i = 0; i < retrieved.size(); i++)
    {
        riverObjects.push_back((retrieved[i]));
    }
}