//
// Created by wiktor on 24.11.2019.
//

#include "MainEndpoint.h"

void MainEndpoint::createObject() {
    cout << "ERROR: You cannot create an object at the top level!" << endl;
}

void MainEndpoint::showObject(int) {
    cout << "ERROR: First you need to specify a class!" << endl;
}

void MainEndpoint::deleteObject(int) {
    cout << "ERROR: You cannot delete any object from the top level!" << endl;
}

MainEndpoint::MainEndpoint(vector<Endpoint*> endpoints) : endpoints(endpoints) {}

void MainEndpoint::showAll() {
//    cout << "List of available classes:" << endl;
    for(int i = 0; i < endpoints.size(); i++) {
        cout << endpoints[i]->getName() << endl;
        endpoints[i]->showAll();
    }
}

string MainEndpoint::getName() {
    return "Transport";
}

void MainEndpoint::saveToFile(string filename) {
    for(int i = 0; i < endpoints.size(); i++) {
        endpoints[i]->saveToFile(filename + "_" + endpoints[i]->getName() + ".txt");
    }
}

void MainEndpoint::readFromFile(string filename) {
    for(int i = 0; i < endpoints.size(); i++) {
        endpoints[i]->readFromFile(filename + "_" + endpoints[i]->getName() + ".txt");
    }
}

void MainEndpoint::modifyObject(int) {
    cout << "First you need to choose a specific class!" << endl;
}
