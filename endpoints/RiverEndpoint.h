//
// Created by wiktor on 23.11.2019.
//

#ifndef PROJEKT_RIVERENDPOINT_H
#define PROJEKT_RIVERENDPOINT_H

#include <vector>
#include <string>
#include "classes/River.h"
#include "Endpoint.h"

class RiverEndpoint : public Endpoint
{
private:
    string parseToString(River &);
    vector<River> parseFromString(string);

public:
    RiverEndpoint();
    vector<River> riverObjects;
    River generateObject();
    string getName();
    void createObject();
    void deleteObject(int);
    void modifyObject(int);
    void showObject(int);
    void showAll();
    void saveToFile(string);
    void readFromFile(string);
};

#endif //PROJEKT_RIVERENDPOINT_H
