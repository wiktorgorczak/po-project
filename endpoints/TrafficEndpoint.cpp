//
// Created by wiktor on 10.12.2019.
//

#include "TrafficEndpoint.h"

TrafficEndpoint::TrafficEndpoint(vector<Endpoint *> endpoints) : MainEndpoint(endpoints){}

string TrafficEndpoint::getName() {
    return "Traffic";
}