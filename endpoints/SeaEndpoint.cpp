//
// Created by wiktor on 23.11.2019.
//

#include "SeaEndpoint.h"

SeaEndpoint::SeaEndpoint() {}

string SeaEndpoint::parseToString(Sea &objectToParse) {
    string content = "TYPE:Sea\n";
    content += "STATE_BUDGET_SPENDING:" + to_string(objectToParse.getStateBudgetSpending()) + "\n";

    list<string>::iterator listIterator;
    content += "WATER_COMPANIES:";
    list<string> companies = objectToParse.getWaterCompanies();
    for(listIterator = companies.begin(); listIterator != companies.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "PLANS:";
    list<string> plans = objectToParse.getPlans();
    for(listIterator = plans.begin(); listIterator != plans.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }

    content += "\nSEA:" + objectToParse.getSea();
    content = content.substr(0, content.size() - 1);

    content += "\nEND:END\n";
    return content;
}

vector<Sea> SeaEndpoint::parseFromString(string content) {
    vector<Sea> result;
    double spending;
    list<string> plans, waterCompanies;
    string sea;

    stringstream ss(content);
    bool ignore = true;
    while(ss.good()) {
        string substr;
        getline(ss, substr, '\n');

        if (!ignore) {
            stringstream substrSs(substr);
            string key, value;
            getline(substrSs, key, ':');
            getline(substrSs, value, ':');

            if (key == "STATE_BUDGET_SPENDING") {
                spending = stod(value);
            } else if (key == "SEA") {
                sea = value;
            } else if (key == "PLANS") {
                insertStringsToList(plans, value, ';');
            } else if (key == "WATER_COMPANIES") {
                insertStringsToList(waterCompanies, value, ';');
            } else if (key == "END") {
                Sea parsed(spending, plans, waterCompanies, sea);
                result.push_back(parsed);

                spending = 0;
                plans.clear();
                waterCompanies.clear();
                sea = "";
            } else {
                ignore = true;
            }

        }
        if (substr == "TYPE:Sea")
            ignore = false;
    }
    return result;
}

Sea SeaEndpoint::generateObject() {
    double spending;
    list<string> plans, waterCompanies;
    string sea;

    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    cout << "Enter state budget spending: " << endl;
    cin >> spending;

    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    cout << "Enter plans separated by semicolon (planA;planB;planC): " << endl;
    string plansStr;
    getline(cin, plansStr);
    insertStringsToList(plans, plansStr, ';');

    cout << "Enter sea: " << endl;
    getline(cin, sea);

    cout << "Enter water companies separated by semicolon (Company1;Company2;Company3): " << endl;
    string companiesStr;
    getline(cin, companiesStr);
    insertStringsToList(waterCompanies, companiesStr, ';');

    Sea object(spending, plans, waterCompanies, sea);
    return object;
}

string SeaEndpoint::getName() {
    return "Sea";
}

void SeaEndpoint::createObject() {
    cout << "Creating new object Sea\n=======" << endl;
    this->seaObjects.push_back(generateObject());
}

void SeaEndpoint::deleteObject(int pos) {
    if(pos >= this->seaObjects.size()) {
        cout << "No such object!" << endl;
        return;
    }

    vector<Sea> *objects = &this->seaObjects;
    objects->erase(objects->begin() + pos);
}

void SeaEndpoint::modifyObject(int pos) {
    if(pos >= this->seaObjects.size()) {
        cout << "No such object!" << endl;
        return;
    }

    cout << "Modifying object from position: " << pos << "\n========";
//    this->seaObjects[pos] = generateObject();
    Sea modified = generateObject();
    this->seaObjects[pos].setPlans(modified.getPlans());
    this->seaObjects[pos].setStateBudgetSpending(modified.getStateBudgetSpending());
    this->seaObjects[pos].setSea(modified.getSea());
}

void SeaEndpoint::showObject(int pos) {
    if(pos >= this->seaObjects.size()) {
        cout <<"No such object!" << endl;
        return;
    }

    Sea *object = &this->seaObjects.at(pos);
    cout << "Information about object no. " << pos << endl;
    cout << "Sea: " << object->getSea() << endl;

    list<string>::iterator it;

    cout << "\t Water companies: " <<endl;
    list<string> companies = object->getWaterCompanies();
    for(it = companies.begin(); it != companies.end(); ++it) {
        cout << "\t\tCompany: " << *it << endl;
    }

    cout << "\t Plans: " <<endl;
    list<string> plans = object->getPlans();
    for(it = plans.begin(); it != plans.end(); ++it) {
        cout << "\t\tPlan: " << *it << endl;
    }
}

void SeaEndpoint::showAll() {
    cout << "All objects of this class: " << endl;
    vector<Sea>::iterator it;

    for(int i = 0; i < seaObjects.size(); i++) {
        cout << "No. " << i << "\n";
        showObject(i);
    }
}

void SeaEndpoint::saveToFile(string filename) {
    string content = "";
    for(Sea sea : seaObjects) {
        content += parseToString(sea);
    }

    try {
        stringToFile(filename, content);
    } catch(string message) {
        cout << message << endl;
    }
}

void SeaEndpoint::readFromFile(string filename) {
    string output;
    try {
        output = fileToString(filename);
    } catch(string message) {
        cout << message << endl;
        return;
    }

    vector<Sea> retrieved = parseFromString(output);
    for(int i = 0; i < retrieved.size(); i++)
    {
        seaObjects.push_back((retrieved[i]));
    }
}