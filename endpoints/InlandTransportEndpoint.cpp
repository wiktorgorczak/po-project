#include "InlandTransportEndpoint.h"

string InlandTransportEndpoint::parseToString(InlandTransport &objectToParse) {
    string content = "TYPE:InlandTransport\n";
    content += "STATE_BUDGET_SPENDING:" + to_string(objectToParse.getStateBudgetSpending()) + "\n";

    list<string>::iterator listIterator;

    content += "TRAFFIC_COMPANIES:";
    list<string> companies = objectToParse.getTrafficCompanies();
    for(listIterator = companies.begin(); listIterator != companies.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "MOTORWAYS:";
    list<string> motorways = objectToParse.getMotorways();
    for(listIterator = motorways.begin(); listIterator != motorways.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "EXPRESSWAYS:";
    list<string> expressways = objectToParse.getExpressways();
    for(listIterator = expressways.begin(); listIterator != expressways.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "NATIONAL_ROADS:";
    list<string> nationalRoads = objectToParse.getNationalRoads();
    for(listIterator = nationalRoads.begin(); listIterator != nationalRoads.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "VOIVODESHIP_ROADS:";
    list<string> voivodeshipRoads = objectToParse.getVoivodeshipRoads();
    for(listIterator = voivodeshipRoads.begin(); listIterator != voivodeshipRoads.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "PLANS:";
    list<string> plans = objectToParse.getPlans();
    for(listIterator = plans.begin(); listIterator != plans.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "RAILWAY_COMPANY:" + objectToParse.getRailwayCompany() + "\n";
    content += "ADMINISTRATIVE_UNIT:" + objectToParse.getAdministrativeUnit() + "\n";
    content += "AVAILABLE_LINES:";

    list<vector<string>>::iterator linesIterator;
    list<vector<string>> availableLines = objectToParse.getAvailableLines();
    for(linesIterator = availableLines.begin();
        linesIterator != availableLines.end();
        ++linesIterator)
    {
        content += linesIterator->at(0) + "," + linesIterator->at(1) + ";";
    }
    content = content.substr(0, content.size() - 1);


    content += "\nEND:END\n";
    return content;
}

vector<InlandTransport> InlandTransportEndpoint::parseFromString(string content) {
    vector<InlandTransport> result;
    double spending;
    list<string> plans, trafficCompanies, motorways, expressways, nationalRoads, voivodeshipRoads;
    string railwayCompany, administrativeUnit;
    list<vector<string>> availableLines;

    stringstream ss(content);
    bool ignore = true;
    while(ss.good())
    {
        string substr;
        getline(ss, substr, '\n');

        if(!ignore)
        {
            stringstream substrSs(substr);
            string key, value;
            getline(substrSs, key, ':');
            getline(substrSs, value, ':');

            if(key == "STATE_BUDGET_SPENDING")
            {
                spending = stod(value);
            }
            else if(key == "ADMINISTRATIVE_UNIT")
            {
                administrativeUnit = value;
            }
            else if(key == "RAILWAY_COMPANY")
            {
                railwayCompany = value;
            }
            else if(key == "PLANS")
            {
                insertStringsToList(plans, value, ';');
            }
            else if(key == "AVAILABLE_LINES")
            {
                parseFromStringToListOfVectors(availableLines, value, ';', ',');
            }
            else if(key == "TRAFFIC_COMPANIES")
            {
                insertStringsToList(trafficCompanies, value, ';');
            }
            else if(key == "MOTORWAYS")
            {
                insertStringsToList(motorways, value, ';');
            }
            else if(key == "EXPRESSWAYS")
            {
                insertStringsToList(expressways, value, ';');
            }
            else if(key == "NATIONAL_ROADS")
            {
                insertStringsToList(nationalRoads, value, ';');
            }
            else if(key == "VOIVODESHIP_ROADS")
            {
                insertStringsToList(voivodeshipRoads, value, ';');
            }
            else if(key == "END")
            {
                InlandTransport parsed(spending, plans, railwayCompany, availableLines, trafficCompanies, motorways, expressways, nationalRoads, voivodeshipRoads, administrativeUnit);
                result.push_back(parsed);

                spending = 0;
                plans.clear();
                availableLines.clear();
                trafficCompanies.clear();
                motorways.clear();
                expressways.clear();
                nationalRoads.clear();
                voivodeshipRoads.clear();
                railwayCompany = "";
                administrativeUnit = "";
            }
            else
            {
                ignore = true;
            }

        }
        if(substr == "TYPE:InlandTransport")
            ignore = false;
    }

    return result;
}

InlandTransportEndpoint::InlandTransportEndpoint() {

}

string InlandTransportEndpoint::getName() {
    return "InlandTransport";
}

InlandTransport InlandTransportEndpoint::generateObject()
{
    double spending;
    list<string> plans;
    string railwayCompany, administrativeUnit;
    list<vector<string>> availableLines;
    list<string> trafficCompanies,
            motorways,
            expressways,
            nationalRoads,
            voivodeshipRoads;

    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    cout << "Enter state budget spending: " << endl;
    cin >> spending;

    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    cout << "Enter plans separated by semicolon (planA;planB;planC): " << endl;
    string plansStr;
    getline(cin, plansStr);
    insertStringsToList(plans, plansStr, ';');

    cout << "Enter administrative unit: " << endl;
    getline(cin, administrativeUnit);

    cout << "Enter railway company: " << endl;
    getline(cin, railwayCompany);

    cout << "Enter lines (stationA,stationB;stationA,stationB;...)" << endl;
    string availableLinesStr;
    getline(cin, availableLinesStr);

    list<string> availableLinesTmp;
    insertStringsToList(availableLinesTmp, availableLinesStr, ';');

    list<string>::iterator it;
    for(it = availableLinesTmp.begin(); it != availableLinesTmp.end(); ++it) {
        vector<string> vec;
        insertStringsToVector(vec, *it, ',');
        availableLines.push_back(vec);
    }

    cout << "Enter traffic companies separated by semicolon (Company1;Company2;Company3): " << endl;
    string companiesStr;
    getline(cin, companiesStr);
    insertStringsToList(trafficCompanies, companiesStr, ';');

    cout << "Enter motorways separated by semicolon (A1;A2;A3): " << endl;
    string motorwaysStr;
    getline(cin, motorwaysStr);
    insertStringsToList(motorways, motorwaysStr, ';');

    cout << "Enter expressways separated by semicolon (S1;S2;S3): " << endl;
    string expresswaysStr;
    getline(cin, expresswaysStr);
    insertStringsToList(expressways, expresswaysStr, ';');

    cout << "Enter national roads separated by semicolon (DK1;DK2;DK3): " << endl;
    string nationalRoadsStr;
    getline(cin, nationalRoadsStr);
    insertStringsToList(nationalRoads, nationalRoadsStr, ';');

    cout << "Enter voivodeship roads separated by semicolon (DW1;DW2;DW3): " << endl;
    string voivodeshipRoadsStr;
    getline(cin, voivodeshipRoadsStr);
    insertStringsToList(voivodeshipRoads, voivodeshipRoadsStr, ';');

    InlandTransport object(spending, plans, railwayCompany, availableLines, trafficCompanies, motorways, expressways, nationalRoads, voivodeshipRoads, administrativeUnit);
    return object;
}

void InlandTransportEndpoint::createObject() {
    cout << "Creating new object InlandTransport\n========" << endl;
    this->inlandTransportObjects.push_back(generateObject());
}

void InlandTransportEndpoint::deleteObject(int pos) {
    if(pos >= this->inlandTransportObjects.size()) {
        cout << "No such object!" << endl;
        return;
    }
    vector<InlandTransport> *objects = &this->inlandTransportObjects;
    objects->erase(objects->begin() + pos);

}

void InlandTransportEndpoint::modifyObject(int pos) {
    if(pos >= this->inlandTransportObjects.size()) {
        cout << "No such object!" << endl;
        return;
    }
    cout << "Modifying object from position " << pos << "\n========" << endl;
    //this->inlandTransportObjects[pos] = generateObject();
    InlandTransport modified = generateObject();
    this->inlandTransportObjects[pos].setRailwayCompany(modified.getRailwayCompany());
    this->inlandTransportObjects[pos].setStateBudgetSpending(modified.getStateBudgetSpending());
    this->inlandTransportObjects[pos].setPlans(modified.getPlans());
    this->inlandTransportObjects[pos].setAdministrativeUnit(modified.getAdministrativeUnit());
}

void InlandTransportEndpoint::showObject(int pos) {
    if(pos >= this->inlandTransportObjects.size()) {
        cout << "No such object!" << endl;
        return;
    }

    InlandTransport *object = &this->inlandTransportObjects.at(pos);
    cout << "Information about object no. " << pos << endl;
    cout << "Railway company: " << object->getRailwayCompany() << endl;
    cout << "Administrative unit: " << object->getAdministrativeUnit() << endl;

    cout << "\tAvailable lines:" << endl;

    list<vector<string>>::iterator linesIterator;
    list<vector<string>> availableLines = object->getAvailableLines();

    for(linesIterator = availableLines.begin(); linesIterator != availableLines.end(); ++linesIterator) {
        cout << "\t\tA: " << linesIterator->at(0) << " B: " << linesIterator->at(1) << endl;
    }

    list<string>::iterator it;

    cout << "\t Traffic companies: " <<endl;
    list<string> companies = object->getTrafficCompanies();
    for(it = companies.begin(); it != companies.end(); ++it) {
        cout << "\t\tCompany: " << *it << endl;
    }

    cout << "\t Plans: " <<endl;
    list<string> plans = object->getPlans();
    for(it = plans.begin(); it != plans.end(); ++it) {
        cout << "\t\tPlan: " << *it << endl;
    }

    cout << "\t Motorways: " <<endl;
    list<string> motorways = object->getMotorways();
    for(it = motorways.begin(); it != motorways.end(); ++it) {
        cout << "\t\tMotorway: " << *it << endl;
    }

    cout << "\t Expressways: " <<endl;
    list<string> expressways = object->getExpressways();
    for(it = expressways.begin(); it != expressways.end(); ++it) {
        cout << "\t\tExpressway: " << *it << endl;
    }

    cout << "\t National roads: " <<endl;
    list<string> nationalRoads = object->getNationalRoads();
    for(it = nationalRoads.begin(); it != nationalRoads.end(); ++it) {
        cout << "\t\tNational road: " << *it << endl;
    }

    cout << "\t Voivodeship roads: " <<endl;
    list<string> voivodeshipRoads = object->getVoivodeshipRoads();
    for(it = voivodeshipRoads.begin(); it != voivodeshipRoads.end(); ++it) {
        cout << "\t\tVoivodeship road: " << *it << endl;
    }
}

void InlandTransportEndpoint::showAll()
{
    cout << "All objects of this class: " << endl;
    vector<InlandTransport>::iterator it;
    for(int i = 0; i < inlandTransportObjects.size(); i++)
    {
        cout << "No. " << i << "\n";
        showObject(i);
    }
}

void InlandTransportEndpoint::saveToFile(string filename) {
    string content = "";
    for(InlandTransport inlandTransport : inlandTransportObjects)
    {
        content += parseToString(inlandTransport);
    }

    try {
        stringToFile(filename, content);
    } catch(string message) {
        cout << message << endl;
    }
}

void InlandTransportEndpoint::readFromFile(string filename) {
    string output;
    try {
        output = fileToString(filename);
    } catch(string message) {
        cout << message << endl;
        return;
    }

    vector<InlandTransport> retrieved = parseFromString(output);
    for(int i = 0; i < retrieved.size(); i++) {
        inlandTransportObjects.push_back(retrieved[i]);
    }
}
