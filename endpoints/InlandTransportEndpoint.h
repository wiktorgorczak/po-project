#ifndef PROJEKT_INLANDTRANSPORTENDPOINT_H
#define PROJEKT_INLANDTRANSPORTENDPOINT_H
#include "Endpoint.h"
#include "classes/InlandTransport.h"
#include <vector>
#include <sstream>
#include <iostream>

class InlandTransportEndpoint : public Endpoint
{
private:
    string parseToString(InlandTransport &);
    vector<InlandTransport> parseFromString(string);
public:
    InlandTransportEndpoint();
    vector<InlandTransport> inlandTransportObjects;
    InlandTransport generateObject();
    string getName();
    void createObject();
    void deleteObject(int);
    void modifyObject(int);
    void showObject(int);
    void showAll();
    void saveToFile(string);
    void readFromFile(string);
};
#endif //PROJEKT_INLANDTRANSPORTENDPOINT_H
