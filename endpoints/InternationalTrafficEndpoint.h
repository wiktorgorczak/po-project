//
// Created by wiktor on 12.11.2019.
//

#ifndef PROJEKT_INTERNATIONALTRAFFICENDPOINT_H
#define PROJEKT_INTERNATIONALTRAFFICENDPOINT_H
#include <string>
#include <vector>
#include "classes/InternationalTraffic.h"
#include "Endpoint.h"

class InternationalTrafficEndpoint : public Endpoint
{
private:
    string parseToString(InternationalTraffic &);
    vector<InternationalTraffic> parseFromString(string);
public:
    InternationalTrafficEndpoint();
    vector<InternationalTraffic> internationalTrafficObjects;
    InternationalTraffic generateObject();
    string getName();
    void createObject();
    void deleteObject(int);
    void modifyObject(int);
    void showObject(int);
    void showAll();
    void saveToFile(string);
    void readFromFile(string);
};

#endif //PROJEKT_INTERNATIONALTRAFFICENDPOINT_H
