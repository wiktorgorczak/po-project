//
// Created by wiktor on 12.11.2019.
//
#include "InternationalTrafficEndpoint.h"

string InternationalTrafficEndpoint::parseToString(InternationalTraffic &objectToParse) {
    /*double stateBudgetSpending,
    list<string> plans,
    list<string> trafficCompanies,
    list<string> motorways,
    list<string> expressways,
    list<string> nationalRoads,
    list<string> voivodeshipRoads,
    string state,
    list<string> borderCrossings,
    list<string> schengen*/
    string content = "TYPE:InternationalTraffic\n";
    content += "STATE_BUDGET_SPENDING:" + to_string(objectToParse.getStateBudgetSpending()) + "\n";

    list<string>::iterator listIterator;

    content += "TRAFFIC_COMPANIES:";
    list<string> companies = objectToParse.getTrafficCompanies();
    for(listIterator = companies.begin(); listIterator != companies.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "MOTORWAYS:";
    list<string> motorways = objectToParse.getMotorways();
    for(listIterator = motorways.begin(); listIterator != motorways.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "EXPRESSWAYS:";
    list<string> expressways = objectToParse.getExpressways();
    for(listIterator = expressways.begin(); listIterator != expressways.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "NATIONAL_ROADS:";
    list<string> nationalRoads = objectToParse.getNationalRoads();
    for(listIterator = nationalRoads.begin(); listIterator != nationalRoads.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "VOIVODESHIP_ROADS:";
    list<string> voivodeshipRoads = objectToParse.getVoivodeshipRoads();
    for(listIterator = voivodeshipRoads.begin(); listIterator != voivodeshipRoads.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "PLANS:";
    list<string> plans = objectToParse.getPlans();
    for(listIterator = plans.begin(); listIterator != plans.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "STATE:" + objectToParse.getState() + "\n";

    content += "BORDER_CROSSINGS:";
    list<string> borderCrossings = objectToParse.getBorderCrossings();
    for(listIterator = borderCrossings.begin(); listIterator != borderCrossings.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);
    content += "\n";

    content += "SCHENGEN:";
    list<string> schengen = objectToParse.getSchengen();
    for(listIterator = schengen.begin(); listIterator != schengen.end(); ++listIterator)
    {
        content += *listIterator + ";";
    }
    content = content.substr(0, content.size() - 1);

    content += "\nEND:END\n";
    return content;
}

vector<InternationalTraffic> InternationalTrafficEndpoint::parseFromString(string content) {
    vector<InternationalTraffic> result;
    double spending;
    list<string> plans, trafficCompanies, motorways, expressways, nationalRoads, voivodeshipRoads, borderCrossings, schengen;
    string state;

    stringstream ss(content);
    bool ignore = true;
    while(ss.good())
    {
        string substr;
        getline(ss, substr, '\n');

        if(!ignore)
        {
            stringstream substrSs(substr);
            string key, value;
            getline(substrSs, key, ':');
            getline(substrSs, value, ':');

            if(key == "STATE_BUDGET_SPENDING")
            {
                spending = stod(value);
            }
            else if(key == "STATE")
            {
                state = value;
            }
            else if(key == "PLANS")
            {
                insertStringsToList(plans, value, ';');
            }
            else if(key == "TRAFFIC_COMPANIES")
            {
                insertStringsToList(trafficCompanies, value, ';');
            }
            else if(key == "MOTORWAYS")
            {
                insertStringsToList(motorways, value, ';');
            }
            else if(key == "EXPRESSWAYS")
            {
                insertStringsToList(expressways, value, ';');
            }
            else if(key == "NATIONAL_ROADS")
            {
                insertStringsToList(nationalRoads, value, ';');
            }
            else if(key == "VOIVODESHIP_ROADS")
            {
                insertStringsToList(voivodeshipRoads, value, ';');
            }
            else if(key == "BORDER_CROSSINGS")
            {
                insertStringsToList(borderCrossings, value, ';');
            }
            else if(key == "SCHENGEN")
            {
                insertStringsToList(schengen, value, ';');
            }
            else if(key == "END")
            {
                InternationalTraffic parsed(spending, plans, trafficCompanies, motorways, expressways, nationalRoads, voivodeshipRoads, state, borderCrossings, schengen);
                result.push_back(parsed);

                spending = 0;
                plans.clear();
                trafficCompanies.clear();
                motorways.clear();
                expressways.clear();
                nationalRoads.clear();
                voivodeshipRoads.clear();
                state = "";
                borderCrossings.clear();
                schengen.clear();
            }
            else
            {
                ignore = true;
            }

        }
        if(substr == "TYPE:InternationalTraffic")
            ignore = false;
    }

    return result;
}

InternationalTrafficEndpoint::InternationalTrafficEndpoint() {

}

InternationalTraffic InternationalTrafficEndpoint::generateObject() {
    double spending;
    list<string> plans, trafficCompanies, motorways, expressways, nationalRoads, voivodeshipRoads, borderCrossings, schengen;
    string state;

    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    cout << "Enter state budget spending: " << endl;
    cin >> spending;

    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    cout << "Enter plans separated by semicolon (planA;planB;planC): " << endl;
    string plansStr;
    getline(cin, plansStr);
    insertStringsToList(plans, plansStr, ';');

    cout << "Enter state: " << endl;
    getline(cin, state);

    cout << "Enter traffic companies separated by semicolon (Company1;Company2;Company3): " << endl;
    string companiesStr;
    getline(cin, companiesStr);
    insertStringsToList(trafficCompanies, companiesStr, ';');

    cout << "Enter motorways separated by semicolon (A1;A2;A3): " << endl;
    string motorwaysStr;
    getline(cin, motorwaysStr);
    insertStringsToList(motorways, motorwaysStr, ';');

    cout << "Enter expressways separated by semicolon (S1;S2;S3): " << endl;
    string expresswaysStr;
    getline(cin, expresswaysStr);
    insertStringsToList(expressways, expresswaysStr, ';');

    cout << "Enter national roads separated by semicolon (DK1;DK2;DK3): " << endl;
    string nationalRoadsStr;
    getline(cin, nationalRoadsStr);
    insertStringsToList(nationalRoads, nationalRoadsStr, ';');

    cout << "Enter voivodeship roads separated by semicolon (DW1;DW2;DW3): " << endl;
    string voivodeshipRoadsStr;
    getline(cin, voivodeshipRoadsStr);
    insertStringsToList(voivodeshipRoads, voivodeshipRoadsStr, ';');

    cout << "Enter border crossings separated by semicolon (City1;Town1;Town2): " << endl;
    string borderCrossingsStr;
    getline(cin, borderCrossingsStr);
    insertStringsToList(borderCrossings, borderCrossingsStr, ';');

    cout << "Enter Schengen separated by semicolon (state1;state2;state3): " << endl;
    string schengenStr;
    getline(cin, schengenStr);
    insertStringsToList(schengen, schengenStr, ';');

    InternationalTraffic object(spending, plans, trafficCompanies, motorways, expressways, nationalRoads, voivodeshipRoads, state, borderCrossings, schengen);
    return object;
}

string InternationalTrafficEndpoint::getName() {
    return "InternationalTraffic";
}

void InternationalTrafficEndpoint::createObject() {
    cout << "Creating new object InternationalTraffic\n========" << endl;
    this->internationalTrafficObjects.push_back(generateObject());
}

void InternationalTrafficEndpoint::deleteObject(int pos) {
    if(pos >= this->internationalTrafficObjects.size()) {
        cout << "No such object!" << endl;
        return;
    }

    vector<InternationalTraffic> *objects = &this->internationalTrafficObjects;
    objects->erase(objects->begin() + pos);
}

void InternationalTrafficEndpoint::modifyObject(int pos) {
    if(pos >= this->internationalTrafficObjects.size()) {
        cout << "No such object!" << endl;
        return;
    }

    cout << "Modifying object from position: " << pos << "\n========";
//    this->internationalTrafficObjects[pos] = generateObject();
    InternationalTraffic modified = generateObject();
    this->internationalTrafficObjects[pos].setPlans(modified.getPlans());
    this->internationalTrafficObjects[pos].setStateBudgetSpending(modified.getStateBudgetSpending());
    this->internationalTrafficObjects[pos].setState(modified.getState());
    this->internationalTrafficObjects[pos].setBorderCrossings(modified.getBorderCrossings());
    this->internationalTrafficObjects[pos].setSchengen(modified.getSchengen());
}

void InternationalTrafficEndpoint::showObject(int pos) {
    if(pos >= this->internationalTrafficObjects.size()) {
        cout <<"No such object!" << endl;
        return;
    }

    InternationalTraffic *object = &this->internationalTrafficObjects.at(pos);
    cout << "Information about object no. " << pos << endl;
    cout << "State: " << object->getState() << endl;

    list<string>::iterator it;

    cout << "\t Traffic companies: " <<endl;
    list<string> companies = object->getTrafficCompanies();
    for(it = companies.begin(); it != companies.end(); ++it) {
        cout << "\t\tCompany: " << *it << endl;
    }

    cout << "\t Plans: " <<endl;
    list<string> plans = object->getPlans();
    for(it = plans.begin(); it != plans.end(); ++it) {
        cout << "\t\tPlan: " << *it << endl;
    }

    cout << "\t Motorways: " <<endl;
    list<string> motorways = object->getMotorways();
    for(it = motorways.begin(); it != motorways.end(); ++it) {
        cout << "\t\tMotorway: " << *it << endl;
    }

    cout << "\t Expressways: " <<endl;
    list<string> expressways = object->getExpressways();
    for(it = expressways.begin(); it != expressways.end(); ++it) {
        cout << "\t\tExpressway: " << *it << endl;
    }

    cout << "\t National roads: " <<endl;
    list<string> nationalRoads = object->getNationalRoads();
    for(it = nationalRoads.begin(); it != nationalRoads.end(); ++it) {
        cout << "\t\tNational road: " << *it << endl;
    }

    cout << "\t Voivodeship roads: " <<endl;
    list<string> voivodeshipRoads = object->getVoivodeshipRoads();
    for(it = voivodeshipRoads.begin(); it != voivodeshipRoads.end(); ++it) {
        cout << "\t\tVoivodeship road: " << *it << endl;
    }

    cout << "\t Countries in the schengen zone: " <<endl;
    list<string> schengen = object->getSchengen();
    for(it = schengen.begin(); it != schengen.end(); ++it) {
        cout << "\t\t" << *it << endl;
    }

    cout << "\t Border crossings: " <<endl;
    list<string> borderCrossings = object->getBorderCrossings();
    for(it = borderCrossings.begin(); it != borderCrossings.end(); ++it) {
        cout << "\t\t" << *it << endl;
    }
}

void InternationalTrafficEndpoint::showAll() {
    cout << "All objects of this class: " << endl;
    for(int i = 0; i < internationalTrafficObjects.size(); i++) {
        cout << "No. " << i << "\n";
        showObject(i);
    }
}

void InternationalTrafficEndpoint::saveToFile(string filename) {
    string content = "";
    for(InternationalTraffic internationalTraffic : internationalTrafficObjects) {
        content += parseToString(internationalTraffic);
    }

    try {
        stringToFile(filename, content);
    } catch(string message) {
        cout << message << endl;
    }
}

void InternationalTrafficEndpoint::readFromFile(string filename) {
    string output;
    try {
        output = fileToString(filename);
    } catch(string message) {
        cout << message << endl;
        return;
    }

    vector<InternationalTraffic> retrieved = parseFromString(output);
    for(int i = 0; i < retrieved.size(); i++)
    {
        internationalTrafficObjects.push_back((retrieved[i]));
    }
}
