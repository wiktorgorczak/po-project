//
// Created by wiktor on 23.11.2019.
//

#ifndef PROJEKT_SEAENDPOINT_H
#define PROJEKT_SEAENDPOINT_H

#include <vector>
#include <string>
#include "classes/Sea.h"
#include "Endpoint.h"

class SeaEndpoint : public Endpoint
{
private:
    string parseToString(Sea &);
    vector<Sea> parseFromString(string);

public:
    SeaEndpoint();
    vector<Sea> seaObjects;
    Sea generateObject();
    string getName();
    void createObject();
    void deleteObject(int);
    void modifyObject(int);
    void showObject(int);
    void showAll();
    void saveToFile(string);
    void readFromFile(string);
};

#endif //PROJEKT_SEAENDPOINT_H
