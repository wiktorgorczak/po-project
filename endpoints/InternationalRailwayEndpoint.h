#ifndef INTERNATIONAL_RAILWAY_ENDPOINT
#define INTERNATIONAL_RAILWAY_ENDPOINT
#include "Endpoint.h"
#include "classes/InternationalRailway.h"
#include <vector>
#include <sstream>
#include <iostream>
class InternationalRailwayEndpoint : public Endpoint
{
private:
    string parseToString(InternationalRailway &);
    vector<InternationalRailway> parseFromString(string);
public:
    InternationalRailwayEndpoint();
    vector<InternationalRailway> internationalRailways;
    InternationalRailway generateObject();
    string getName();
    void createObject();
    void deleteObject(int);
    void modifyObject(int);
    void showObject(int);
    void showAll();
    void saveToFile(string);
    void readFromFile(string);
};

#endif